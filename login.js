import { View, Text, Image, Dimensions, Button, TextInput } from 'react-native'
import React from 'react'
import { styles } from './latihan-styles'

const win = Dimensions.get('window');
const Login = () => {
  return (
    <View style={styles.container}>
      <View style={styles.section1}>
        <View style={styles.header}>
          <Text style={styles.headerText}>Login</Text>
        </View>
        <View style={styles.mainContainer}>
          <Image
            style={{
              width: win.width / 2,
              height: win.width / 2,
              resizeMode: "contain",
              alignSelf: "center",
              borderWidth: 1,
              borderRadius: 20,
            }}
            resizeMode="cover"
            source={{
              uri: 'https://upload.wikimedia.org/wikipedia/commons/b/bc/Telkomsel_2021_icon.svg',
            }}
          />
        </View>
      </View>
      <View style={styles.section2}>
        <View style={styles.mainContent}>
          <Text style={styles.mainContentText}>Login with your Credential</Text>
        </View>
        <View>
          <TextInput
            style={styles.textInput}
            placeholder='Username'
          />
          <TextInput
            style={styles.textInput}
            placeholder='Password'
          />
        </View>
        <View style={styles.mainButton}>
          <Button
            title='Sign In'
            onPress={() => console.log('Button Pressed')}
          />
        </View>

      </View>
    </View>
  )
}

export default Login