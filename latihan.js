import { View, Text } from 'react-native'
import React from 'react'
import { styles } from './latihan-styles'

const Latihan = () => {
  return (
    <View style={styles.container}
    // style={{
    //   flexDirection: "column",
    //   height: 100,
    //   padding: 20,
    //   flex: 1,
    // }}
    >
      <View style={styles.section1}>
        <Text style={{ fontSize: 24 }}>Container 1</Text>
      </View>
      <View style={{ backgroundColor: "red", flex: 1 }}>
        <Text>Container 2</Text>
      </View>
    </View>
  )
}

export default Latihan