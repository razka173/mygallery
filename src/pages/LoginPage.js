import React, { useState } from 'react'
import { View, Text, Image, Dimensions, Button, TextInput, Alert, TouchableOpacity } from 'react-native'
import { styles } from '../styles/login-page-style';
import AsyncStorage from '@react-native-community/async-storage';
import { useNavigation } from '@react-navigation/native';

const win = Dimensions.get('window');
const LoginPage = () => {
  const navigation = useNavigation();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [myData, setMyData] = useState({});

  console.log(username);

  const getApiData = async () => {
    if (username === '' || password === '') {
      console.error('Username or password not filled');
      return null;
    }
    await AsyncStorage.removeItem('token');

    try {
      // setIsLoading(true);
      const getData = await fetch(
        'https://playgroundapi.com/bootcamp/api/web/user/login',
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            username: username,
            password: password,
          }),
        },
      );
      console.log(getData);
      if (getData.status === 200) {
        const results = await getData.json();
        setMyData(results);
        // setIsLoading(false);
        await AsyncStorage.setItem('token', results.data.token);
        navigation.navigate('HomePage', results.data);
      } else {
        Alert.alert('Gagal Login', "Username atau password salah");
      }
    } catch (e) {
      console.error(e);
    }

    return null;
  };

  return (
    <View style={styles.container}>
      <View style={styles.section1}>
        <View style={styles.mainContainer}>
          <Image
            style={{
              width: win.width / 2,
              height: win.width / 2,
              resizeMode: "contain",
              alignSelf: "center",
              borderWidth: 1,
              borderRadius: 20,
            }}
            resizeMode="cover"
            source={{
              uri: 'https://upload.wikimedia.org/wikipedia/commons/3/37/Lambang_UNJ_dan_moto.png',
            }}
          />
        </View>
      </View>
      <View style={styles.section2}>
        <View style={styles.mainContent}>
          <Text style={styles.mainContentText}>Login with your Credential</Text>
        </View>
        <View>
          <TextInput
            style={styles.textInput}
            placeholder='Username'
            value={username}
            onChangeText={(e) => setUsername(e)}
          />
          <TextInput
            style={styles.textInput}
            placeholder='Password'
            value={password}
            onChangeText={(e) => setPassword(e)}
            secureTextEntry
          />
        </View>
        <View style={styles.mainButton}>
          <TouchableOpacity
            style={styles.signInButton}
            onPress={() => getApiData()}
          >
            <Text>SIGN IN</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}

export default LoginPage;