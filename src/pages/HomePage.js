import React, { useEffect, useState } from "react"
import { View, Text, StyleSheet, Image, SafeAreaView, FlatList, Dimensions, TouchableOpacity, Button } from "react-native"
import AsyncStorage from '@react-native-community/async-storage';
import { useNavigation } from '@react-navigation/native';

const HomePage = ({ route }) => {
  const navigation = useNavigation();
  const [token, setToken] = useState('');
  const [myData, setMyData] = useState({});
  const profile = route.params

  const gambarThumbnail = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => { navigation.navigate('DetailPage', item); }}
      >
        <Image
          style={styles.imageSize}
          source={{
            uri: item.full_image_url
          }}
        />
      </TouchableOpacity>
    )
  };

  const Profile = ({ profile }) => {
    return (
      <View style={styles.profileBox}>
        <View style={styles.profileImageBox}>
          <Image
            style={styles.profileImage}
            source={{
              uri: profile.avatar_url
            }}
          />
        </View>
        <View style={styles.profileNameBox}>
          <View style={{ display: "flex", flexDirection: "column", flex: 1 }}>
            <View style={{ flex: 1 }}>
              <Text style={styles.profileName}>
                {profile.username}
              </Text>
            </View>
            <View style={{ flex: 1 }}>
              <TouchableOpacity
                style={styles.uploadButton}
              >
                <Text>Upload</Text>
              </TouchableOpacity>
            </View>
            <View style={{ flex: 1 }}>
              <TouchableOpacity
                style={styles.signOutButton}
                onPress={() => { navigation.navigate('LoginPage'); }}
              >
                <Text>Sign Out</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }

  useEffect(() => {
    getApiData();
  }, [])

  const getApiData = async () => {
    const localToken = await AsyncStorage.getItem('token');
    setToken(localToken);

    if (localToken) {
      try {
        // setIsLoading(true);
        const getData = await fetch(
          'https://playgroundapi.com/bootcamp/api/web/posting/list-posting?page=0',
          {
            method: 'GET',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localToken}`
            },
          },
        );
        // console.log(getData);
        if (getData.status === 200) {
          const results = await getData.json();
          setMyData(results.data);
          // setIsLoading(false);
        }
      } catch (e) {
        console.error(e);
      }
    }

    return null;
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.profileContainer}>
        <Profile profile={profile} />
      </View>
      <View style={styles.viewFlatListContainer}>
        <FlatList
          data={myData}
          renderItem={gambarThumbnail}
          keyExtractor={item => item.id}
          numColumns={3}
          columnWrapperStyle={styles.flatListContainer}
        />
      </View>
    </SafeAreaView>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-between",
  },
  profileContainer: {
    flex: 1,
    paddingHorizontal: 24,
    backgroundColor: "whitesmoke",
  },
  profileBox: {
    flexDirection: "row"
  },
  profileImageBox: {
    flex: 2,
    // backgroundColor: "red",
  },
  profileNameBox: {
    flex: 4,
    // backgroundColor: "green",
  },
  profileImage: {
    height: Dimensions.get('window').width * 0.27,
    width: Dimensions.get('window').width * 0.27,
    borderRadius: 50,
    marginVertical: 10,
    marginLeft: "auto",
    marginRight: "auto",
  },
  profileName: {
    fontWeight: "bold",
    fontSize: 20,
    // backgroundColor: "red",
    marginTop: "auto",
    marginBottom: "auto",
  },
  uploadButton: {
    alignItems: "center",
    backgroundColor: "lightblue",
    padding: 10,
    borderRadius: 15,
  },
  signOutButton: {
    alignItems: "center",
    backgroundColor: "pink",
    padding: 10,
    borderRadius: 15,
  },
  viewFlatListContainer: {
    flex: 4,
    backgroundColor: "white",
  },
  flatListContainer: {
    flex: 1,
    marginTop: 12,
    justifyContent: 'space-around',
    paddingHorizontal: 5,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  imageSize: {
    height: Dimensions.get('window').width * 0.27,
    width: Dimensions.get('window').width * 0.27,
    borderRadius: 10,
    marginVertical: 10,
  },
});

export default HomePage;