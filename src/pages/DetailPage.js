import React from "react"
import { View, Text, Image, StyleSheet, Dimensions } from "react-native"

const DetailPage = ({ route }) => {
  const itemDetail = route.params;
  // console.log(itemDetail);

  return (
    <View style={styles.container}>
      <Image
        style={styles.imageSize}
        source={{
          uri: itemDetail.full_image_url
        }}
      />
      <Text style={styles.title}>{itemDetail.title}</Text>
      <Text style={styles.description}>{itemDetail.description}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 16
  },
  imageSize: {
    height: Dimensions.get('window').width * 0.9,
    width: Dimensions.get('window').width * 0.9,
    borderRadius: 10,
    marginVertical: 10,
    marginRight: "auto",
    marginLeft: "auto",
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 10,
  },
  description: {

  },
});

export default DetailPage;