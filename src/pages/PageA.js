import React from "react"
import { View, Text } from "react-native"

const PageA = () => {
  return <View>
    <Text>Ini page A</Text>
  </View>
}

export default PageA;