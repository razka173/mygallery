import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    flex: 1,
  },
  section1: {
    backgroundColor: "white",
    flex: 3,
  },
  section2: {
    backgroundColor: "lightblue",
    flex: 2,
  },
  headerText: {
    fontSize: 24
  },
  header: {
    padding: 20,
    borderBottomColor: 'black',
    borderWidth: 1
  },
  mainContainer: {
    flex: 1,
    backgroundColor: "whitesmoke",
    alignItems: "center",
    justifyContent: "center"
  },
  mainContent: {
    padding: 15,
  },
  mainContentText: {
    fontSize: 20,
    color: "white",
  },
  textInput: {
    margin: 10,
    backgroundColor: "white",
    borderColor: "black",
    borderWidth: 1,
    padding: 10,
  },
  mainButton: {
    margin: 10,
  },
  signInButton: {
    alignItems: "center",
    backgroundColor: "lightgreen",
    padding: 10,
    borderRadius: 10,
  }
})