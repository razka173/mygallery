import React from 'react';

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import PageA from "./pages/PageA";
import LoginPage from './pages/LoginPage';
import HomePage from './pages/HomePage';
import DetailPage from './pages/DetailPage';

const Stack = createNativeStackNavigator();

const Routes = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="LoginPage">
        <Stack.Screen name="PageA" component={PageA} options={{ title: 'Page A' }} />
        <Stack.Screen name="LoginPage" component={LoginPage} options={{ title: 'Login' }} />
        <Stack.Screen name="HomePage" component={HomePage} options={{ title: 'My home' }} />
        <Stack.Screen name="DetailPage" component={DetailPage} options={{ title: 'Detail photo' }} />
        {/* <Stack.Screen name="UploadPage" component={UploadPage} /> */}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;